﻿using System;
using System.Collections.Generic;
using AssoziativSpeicher;

namespace AssoziativeSpeicherKonsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input1 = {1, 1, 1};
            int[] input2 = {1, -1, 1};
            int[] input3 = {1, 1, -1};

            int[] output1 = {1, 1, 1, -1};
            int[] output2 = {-1, -1, 1, 1};
            int[] output3 = {-1, 1, 1, 1};
            
            var trainingsMuster = new List<TrainingsMuster>
            {
                new TrainingsMuster(input1, output1),
                new TrainingsMuster(input2, output2),
                new TrainingsMuster(input3, output3)
            };

            var speicher = new Speicher();
            speicher.GeneriereMatrix(trainingsMuster);

            var ausgabeVektor = "";
            var res = speicher.BerechneAusgabe(input1, out ausgabeVektor);
            Console.WriteLine(ausgabeVektor);
            
            res = speicher.BerechneAusgabe(input2, out ausgabeVektor);
            Console.WriteLine(ausgabeVektor);
            
            res = speicher.BerechneAusgabe(input3, out ausgabeVektor);
            Console.WriteLine(ausgabeVektor);
        }
    }
}