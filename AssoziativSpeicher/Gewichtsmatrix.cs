using System;
using NeuronaleNetzeInterfaces;
using System.Collections.Generic;

namespace AssoziativSpeicher
{
    /// <summary>
    /// Die Gewichtsmatrix hält die einzelnen Verbindungsgewichtungen der Neuronen in Form einer Matrix (2D Array).
    /// </summary>
    public class Gewichtsmatrix: IGewichtsmatrix
    {
        private double[,] _matrix;
        
        /// <summary>
        /// Die Anzahl an Spalten.
        /// </summary>
        public int Spalten { get; }
        
        /// <summary>
        /// Die Anzahl an Zeilen.
        /// </summary>
        public int Zeilen { get; }
        
        public Enums.GewichtsmatrixModus Modus { get; set; }

        /// <summary>
        /// Initialisiert eine Gewichtsmatrix aus Nullen mit der angegebenen Anzahl an Zeilen und Spalten. 
        /// </summary>
        /// <param name="zeilen">Die Anzahl der Zeilen.</param>
        /// <param name="spalten">Die Anzahl der Spalten.</param>
        public Gewichtsmatrix(int zeilen, int spalten)
        {
            Spalten = spalten;
            Zeilen = zeilen;
            // Initialisiert einen zweidimensionalen Array
            _matrix = new double[zeilen,spalten];
            
            // Die Matrix mit 0 füllen
            for (int y = 0; y < zeilen; y++)
            {
                for (int x = 0; x < spalten; x++)
                {
                    _matrix[x, y] = 0;
                }
            }
        }

        /// <summary>
        /// Addiert eine Gewichtsmatrix auf die Gewichtungen dieser Instanz.
        /// </summary>
        /// <param name="a">Die Gewichtsmatrix die addiert werden soll.</param>
        /// <exception cref="ArgumentException">Wird gefeuert wenn die Matrix Dimensionen inkompatibel sind.</exception>
        public void add(Gewichtsmatrix a)
        {
            // TODO: Funktioniert nicht
            if (a.Spalten != Spalten && a.Zeilen != Zeilen)
            {
                // Matrix dimensionen sind inkomptaibel
                throw new ArgumentException();
            }

            for (var y = 0; y < Zeilen; y++)
            {
                for (var x = 0; x < Spalten; x++)
                {
                    this[y, x] += a[y, x];
                }
            } 
        }

        /// <summary>
        /// Initialisiert eine neue Gewichtsmatrix basierend auf einer 2D Liste
        /// </summary>
        /// <param name="listMatrix">Die Liste mit der die Matrix Initialisiert werden soll.</param>
        public Gewichtsmatrix(List<List<int>> listMatrix)
        {
            // zeilen, spalten
            Zeilen = listMatrix.Count;
            Spalten = listMatrix[0].Count;

            _matrix = new double[listMatrix.Count, listMatrix[0].Count];
            
            var r = 0;
            foreach (var row in listMatrix)
            {
                var z = 0;
                foreach (var value in row)
                {
                    _matrix[r, z] = value;
                    z++;
                }

                r++;
            }
        }
        
        public object Clone()
        {
            throw new System.NotImplementedException();
        }

        public string Speichern(string dateiname)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Im Subscript der Gewichtsmatrix können 2 Parameter übergeben werden, welche den Gewichtungswert an einer
        /// bestimmten Stelle in der Matrix zurück gibt oder einen neuen Wert an dieser Stelle einträgt.
        /// </summary>
        /// <param name="a">Die Zeile des Wertes.</param>
        /// <param name="b">Die Spalte des Wertes.</param>
        public double this[int a, int b]
        {
            get => _matrix[a, b];
            set => _matrix[a, b] = value;
        }
    }
}