using System;

namespace AssoziativSpeicher
{
    /// <summary>
    /// Das Neuron wird in einem Netzwerk zum weitergeben von Werten verwendet.
    /// Es wird lediglich der NettoInput des Neurons gesetzt. Die restlichen Werte können hinterher durch
    /// ausführen der implemntierten Funktionen berechnet werden.
    /// Der Nutzer sollte nicht von der Klasse gebrauch machen müssen. 
    /// </summary>
    public class Neuron
    {
        public int NettoInput;
        public int Aktivierung;
        public int Ausgabe;

        public void Aktivierungsfunktion()
        {
            Aktivierung = Math.Sign(NettoInput);
        }

        public void Ausgabefunktion()
        {
            Ausgabe = Aktivierung;
        }
    }
}