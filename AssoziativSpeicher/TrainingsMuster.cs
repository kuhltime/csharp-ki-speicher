using System.Collections.Generic;

namespace AssoziativSpeicher
{
    /// <summary>
    /// Die Trainingsmuster-Klasse dient als einheitliches Objekt zur einfachen Verarbeitung der Trainingsmuster in einem Netzwerk.
    /// </summary>
    public class TrainingsMuster
    {
        private int[] _eingabevektor;
        private int[] _tragetvektor;

        /// <summary>
        /// Initialisiere einen neues Traingsmuster mit dem jeweiligen Eingabevektor und Ausgabevektor.
        /// </summary>
        /// <param name="eingabevektor"></param>
        /// <param name="tragetvektor"></param>
        public TrainingsMuster(int[] eingabevektor, int[] tragetvektor)
        {
            _eingabevektor = eingabevektor;
            _tragetvektor = tragetvektor;
        }

        /// <summary>
        /// Gibt die Anzahl der Elemente von sowohl Eingabe- als auch Ausgabevektor in Form eines Tupels zurück.
        /// </summary>
        public (int eingabe, int target) Dimension => (_eingabevektor.Length, _tragetvektor.Length);

        /// <summary>
        /// Berechnet die Teilmatrix des Traingsmusters. Welche hinterher zum trainieren eines Netzwerkes relevant ist.
        /// </summary>
        /// <returns>Gibt die Teilmatrix in Form einer Gewichtsmatrix zurück.</returns>
        public Gewichtsmatrix Teilmatrix()
        {
            // Eingabevektor_T * Targetvektor
            // Zeile mal Spalte
            
            var rows = new List<List<int>>();
            for (var i = 0; i < Dimension.eingabe; i++)
            {
                var row = new List<int>();
                for (var j = 0; j < Dimension.target; j++)
                {
                    var value = _eingabevektor[i] * _tragetvektor[j];
                    row.Add(value);
                }

                rows.Add(row);
            }

            return new Gewichtsmatrix(rows);
        }
    }
}