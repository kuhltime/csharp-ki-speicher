using System;
using System.Collections.Generic;

namespace AssoziativSpeicher
{
    /// <summary>
    /// Das Speicher Objekt handhabt das Generieren und Trainieren des Assozitativspeichers. 
    /// </summary>
    public class Speicher
    {
        private List<Neuron> EingabeNeuronen = new List<Neuron>();
        private List<Neuron> AusgabeNeuronen = new List<Neuron>();
        
        private Gewichtsmatrix _matrix;
        private List<Gewichtsmatrix> _teilmatrizen = new List<Gewichtsmatrix>();
        
        /// <summary>
        /// Generiert die benötigt Anzahl an Eingabe- und Ausgabeneuronen.
        /// Zudem wird eine leere Gewichtsmatrix generiert.
        /// </summary>
        /// <param name="trainingsMuster">Eine Liste an Trainingsmustern die benutzt wird um die Parameter des
        /// Speichers einzustellen.</param>
        /// <exception cref="ArgumentException">Gibt eine ArgumentException aus, wenn ein Muster aus der Liste
        /// an Trainingsmustern nicht die erforderliche Anzahl an Werten hat.</exception>
        public void GeneriereMatrix(List<TrainingsMuster> trainingsMuster) {
            // Methode soll die Gewichtsmatrix berechnen
            
            // 1a) ===============
            // Validieren der trainingsMuster
            var dimension = trainingsMuster[0].Dimension;
            
            foreach (var muster in trainingsMuster)
            {
                // Wenn das Muster nicht die gewollte Größe hat
                if (muster.Dimension != dimension)
                {
                    throw new ArgumentException();
                }
                
                // 1c) =============== 
                // Für jedes Trainingsmuster eine Teilmatrix erzeugen
                _teilmatrizen.Add(muster.Teilmatrix());
                // ===================
            }
            // ===================
            

            // 1b) ===============
            for (int i = 0; i < dimension.eingabe; i++)
            {
                var neuron = new Neuron();
                EingabeNeuronen.Add(neuron);
            }

            for (int i = 0; i < dimension.target; i++)
            {
                var neuron = new Neuron();
                AusgabeNeuronen.Add(neuron);
            }
            // ===================
            
            // 1d) ===============
            _matrixBerechnen();
            // ====================
        }

        // 1d) ===============
        private void _matrixBerechnen()
        {
            // Initialisiert die Gewichtsmatrix
            _matrix = _teilmatrizen[0];
            for (int i = 1; i < _teilmatrizen.Count; i++)
            {
                // Addiert die Teilmatrizen auf die Gewichtsmatrix
                _matrix.add(_teilmatrizen[i]);
            }
        }
        // ====================

        /// <summary>
        /// Berechnet für einen Eingabevektor den entsrpechenden Ausgabevektor.
        /// </summary>
        /// <param name="eingabeVektor">Der Eingabevektor der durch den Assoziativspeicher "gejagt" werden soll.</param>
        /// <param name="ausgabeVektor">Ein Verweiß auf den Auszugebenden Ausgabevektor</param>
        /// <returns></returns>
        /// <exception cref="Exception">Wirft eine Exception aus wenn die Länge des Eingabevektors nicht mit der Länge
        /// der Eingabeneuronen übereinstimmt.</exception>
        public List<int> BerechneAusgabe(int[] eingabeVektor, out string ausgabeVektor)
        {
            if (eingabeVektor.Length != EingabeNeuronen.Count)
            {
                throw new Exception("Die Anzahl an Eingabe Neuronen stimmt nicht mit dem EingabeVektor überein.");
            }

            for (var i = 0; i < EingabeNeuronen.Count; i++)
            {
                // 2a) ===============
                EingabeNeuronen[i].NettoInput = eingabeVektor[i];
                // ===================
                
                // 2b) ===============
                EingabeNeuronen[i].Aktivierungsfunktion();
                EingabeNeuronen[i].Ausgabefunktion();
                // ===================
            }
            
            // 2d) ===============
            for (int i = 0; i < AusgabeNeuronen.Count; i++)
            {
                // 2c) ===============
                AusgabeNeuronen[i].NettoInput = 0;
                // ===================
                
                for (int j = 0; j < EingabeNeuronen.Count; j++)
                {
                    AusgabeNeuronen[i].NettoInput += EingabeNeuronen[j].Ausgabe * (int) _matrix[j, i];
                }
            }
            // ===================
            
            
            var ergebnis = new List<int>();
            for (var i = 0; i < AusgabeNeuronen.Count; i++)
            {
                // 2e) ===============
                AusgabeNeuronen[i].Aktivierungsfunktion();
                AusgabeNeuronen[i].Ausgabefunktion();
                // ===================

                // 2f) ===============
                ergebnis.Add(AusgabeNeuronen[i].Aktivierung);
                // ===================
            }

            // 2g) ===============
            var ausgabeStr = "[";
            for (var i = 0; i < AusgabeNeuronen.Count; i++)
            {
                var ausgabeNeuron = AusgabeNeuronen[i];
                if (i != AusgabeNeuronen.Count - 1)
                {
                    ausgabeStr += $"{ausgabeNeuron.Aktivierung}, ";
                }
                else
                {
                    ausgabeStr += $"{ausgabeNeuron.Aktivierung}]";
                }
            }
            // ===================

            ausgabeVektor = ausgabeStr;

            // 2f) ===============
            return ergebnis;
            // ===================
        }
    }
}