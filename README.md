# Assoziativ Speicher 

## Trainingsmuster

### Konstruktor

Die `TrainingsMuster`-Klasse dient als einfache Verwaltung für einen Eingangsvektor und den dazu gehörigen Tragetvektor des Trainingsmusters. Sowohl Eingangs- als auch Tragetvektor sind aus Geschwindigkeits Gründen als ein Array aus Integern zu definieren und in den Konstruktor des `TrainingsMuster`s zu übergeben.

```c#
public TrainingsMuster(int[] eingabeVektor, int[] ausgabeVektor)
```



### Vektor Dimensionen

Über das Klassenparameter `Dimension` kann der Nutzer sich die Dimension, bestehende aus einem Tupel mit der Länge des Eingabevektors und der Länge des Ausgabevektors zurück geben lassen.

```c#
public (int eingabe, int target) Dimension
```



### *Teilmatrix*

Zum Trainieren des Assoziativ Speichers wird eine Teilmatrix des `TrainingsMuster`s benötigt. Diese Teilmatrix bildet sich durch das Skalarprodukt des Eingabevektor mal dem transponierten Ausgabevektors. Die zurückgegebene `Gewichtsmatrix` kann nach der Berechnung zur Ermittlung der gesamt Gewichtung nach durchlauf allers `TrainingsMuster` verwendet werden. 

```c#
public Gewichtsmatrix Teilmatrix()
```



## Gewichtsmatrix

Die `Gewichtsmatrix` leitet sich aus der Interface `IGewichtsmatrix` aus der `NeuronaleNetzeInterfaces`-Bibliothek ab. In ihr befindet sich ein zwei dimensionaler Array zum speichern der Matrix Werte.



### Matrix Initialisieren

Eine `Gewichtsmatrix` kann auf zwei wegen Initialisiert werden. 

Zum einen kann mittels übergebener Parameter (Zeilen, Spalten) eine mit nullen gefüllte `Gewichtsmatrix` erstellt werden.

```c#
public Gewichtsmatrix(int zeilen, int spalten)
```

Und zum anderen kann mittels einer zweidimensionalen `List`e eine Matix mit gegebenen Werten erzeugt werden. Diese Initialisierungsmethode wird von der `Teilmatrix()`-Methode aus der `TrainingsMuster`-Klasser verwendet.

```c#
public Gewichtsmatrix(List<List<int>> listMatrix)
```



### Index

Die Klasse verfügt über einen öffentlichen Indexer welcher Werte im Subscript eines Objektes dieser Klasse erlaubt und als resultat, je nach Anweisung einen Wert an gewünschter Position zurück gibt oder den Wert an der gewünschten Position in der Matrix einsetzt. 

**Es ist zu beachten, dass hier x der Index der Reihe und y der Index der Spalte ist.**

```c#
public double this[int x, int y]
```

Beispiel möchte man den Wert an der Matrixstelle (3,2) haben so wäre es beim aufrufen dieses Wertes:

```c#
Gewichtsmatrix matrix;
```



### Matrix addieren

Um im `Speicher` die Matrix zu berechnen gibt es in der `Gewichtsmatrix` eine Additionsmethode welche die aktuelle Matrix mit einer anderen Matrix addiert.

```c#
public void add(Gewichtsmatrix a)
```



## Speicher

Die Speicherklasse ist das eingentliche Herzstück des Assoziativspeichers. Sie beinhaltet die eigentliche `Gewichtsmatrix` des Speichers und verfügt darüber hinaus über die Funktionen zum Trainieren der `Gewichtsmatrix` und des "Feed Forward"-Prozesses des Speichers.



### Trainieren

Das trainieren des Speichers geschieht durch übergeben einer Liste an Trainingsmuster an folgende Funktion:

```c#
public void GeneriereMatrix(List<TrainingsMuster> trainingsMuster)
```

Durch ausführen der Funktion wird jegliche Teilmatrix der Trainingsmuster bestimmt zu einer einzigen `Gewichtungsmatrix` im `Speicher` zusammenaddiert.



### Feed Forward

Nachdem der `Speicher` trainiert wurde kann nun die "Feed Forward"-Funktion des Netzwerkes verwendet werden. Über die Methode:

```c#
public List<int> BerechneAusgabe(int[] eingabeVektor, out string ausgabeVektor)
```

lässt sich das Ergebnis des AssoziativSpeichers sowohl als `List<int>` zurück geben aber auch als eine vorher initialisierten Ausgabestring. 
